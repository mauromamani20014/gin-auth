package entities

import "time"

type User struct {
  UserID    uint      `json:"user_id"    gorm:"primaryKey"`
  FirstName string    `json:"first_name"`
  Email     string    `json:"email"      gorm:"unique"`
  Password  string    `json:"password"`
  CreatedAt time.Time `json:"created_at"`
  UpdatedAt time.Time `json:"updated_at"`
}
