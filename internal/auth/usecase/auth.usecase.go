package usecase

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"

	"gitlab.com/mauromamani20014/gin-auth/internal/auth"
	"gitlab.com/mauromamani20014/gin-auth/internal/entities"
	"gitlab.com/mauromamani20014/gin-auth/pkg/httpErrors"
	"gitlab.com/mauromamani20014/gin-auth/pkg/utils"
)

type AuthUseCase struct {
  authRepository auth.Repository
}

func NewAuthUseCase(authRepository auth.Repository) auth.UseCase {
  return &AuthUseCase{authRepository: authRepository}
}

func (u *AuthUseCase) Register(c *gin.Context, user *entities.User) (*entities.User, error) {
  existsUser, err := u.authRepository.GetByEmail(user)
  if existsUser != nil || err == nil {
    return nil, httpErrors.NewRestError(http.StatusBadRequest, httpErrors.ErrEmailAlreadyExists, nil)
  }

  createdUser, err := u.authRepository.Create(user)
  if err != nil {
    return nil, err
  }

  token, err := utils.GenerateJWT(createdUser)
  if err != nil {
    return nil, httpErrors.NewInternalServerError(errors.Wrap(err, "userUseCase.Create.GenerateJWT"))
  }

  // Configure cookie
  utils.ConfigureJWTCookie(c, token) 

  return createdUser, nil
}

func (u *AuthUseCase) Login(c *gin.Context, user *entities.User) (*entities.User, error) {
  existsUser, err := u.authRepository.GetByEmail(user)
  if err != nil {
    return nil, httpErrors.NewUnauthorizedError(errors.Wrap(err, "authUC.GetUsers.GetByEmail"))
  }

  if existsUser.Password != user.Password {
    return nil, httpErrors.NewRestErrorWithMessage(http.StatusBadRequest, "bad password!", err)
  }

  token, err := utils.GenerateJWT(existsUser)
  if err != nil {
    return nil, httpErrors.NewInternalServerError(errors.Wrap(err, "userUseCase.Create.GenerateJWT"))
  }

  // Configure cookie
  utils.ConfigureJWTCookie(c, token) 

  return existsUser, nil
}

func (u *AuthUseCase) GetByID(userId uint) (*entities.User, error) {
  user, err := u.authRepository.GetById(userId)
  if err != nil {
    return nil, err
  }

  return user, nil
}
