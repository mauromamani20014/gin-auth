package auth

import "gitlab.com/mauromamani20014/gin-auth/internal/entities"

type Repository interface {
  Create(user *entities.User) (*entities.User, error)
  GetById(userId uint) (*entities.User, error)
  GetByEmail(user *entities.User) (*entities.User, error)
}
