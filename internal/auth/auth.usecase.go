package auth

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-auth/internal/entities"
)

type UseCase interface {
  Register(c *gin.Context, user *entities.User) (*entities.User, error)
  Login(c *gin.Context, user *entities.User) (*entities.User, error)
  GetByID(userId uint) (*entities.User, error)
}
