package http

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-auth/internal/auth"
	"gitlab.com/mauromamani20014/gin-auth/internal/entities"
	"gitlab.com/mauromamani20014/gin-auth/pkg/httpErrors"
	"gitlab.com/mauromamani20014/gin-auth/pkg/utils"
)

type authHandlers struct {
  authUseCase auth.UseCase
}

func NewAuthHandlers(authUseCase auth.UseCase) auth.Handlers {
  return &authHandlers{authUseCase: authUseCase}
}

func (h *authHandlers) Register(c *gin.Context) {
  user := &entities.User{}

  c.Bind(user)

  createdUser, err := h.authUseCase.Register(c, user)
  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusCreated, createdUser)
}

func (h *authHandlers) Login(c *gin.Context) {
  type Login struct {
    Email    string `json:"email"`
    Password string `json:"password"`
  }

  login := &Login{}
  c.Bind(login)

  user, err := h.authUseCase.Login(c, &entities.User{
    Email:    login.Email,
    Password: login.Password,
  })

  if err != nil {
    c.JSON(httpErrors.ErrorResponse(err))
    return
  }

  c.JSON(http.StatusOK, user)
}

func (h *authHandlers) Logout(c *gin.Context) {
  if _, err := c.Cookie("jwt"); err != nil {
    if errors.Is(err, http.ErrNoCookie) {
      c.JSON(http.StatusUnauthorized, httpErrors.NewUnauthorizedError(err))
      return
    }
    c.JSON(http.StatusInternalServerError, httpErrors.NewInternalServerError(err))
    return
  }

  utils.DeleteJWTCookie(c)

  c.JSON(http.StatusOK, "LOGOUT")
}

func (h *authHandlers) GetMe(c *gin.Context) {
  user, exists := c.Get("user")
  if !exists {
    c.JSON(http.StatusBadRequest, httpErrors.NewRestErrorWithMessage(
      http.StatusBadRequest,
      "Unable to extract user from request context for unknown reason",
      exists,
    ))
    return
  }

  c.JSON(http.StatusOK, gin.H{
    "user": user,
  })
}
