package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-auth/internal/auth"
	"gitlab.com/mauromamani20014/gin-auth/internal/middlewares"
)

func MapAuthRoutes(authGroup *gin.RouterGroup, h auth.Handlers, mw *middlewares.MiddlewareManager) {
  authGroup.POST("/register", h.Register)
  authGroup.POST("/login", h.Login)
  authGroup.POST("/logout", h.Logout)

  authGroup.Use(mw.AuthJWTMiddleware())
  authGroup.GET("/me", h.GetMe)
}

