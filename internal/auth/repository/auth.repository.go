package repository

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"

	"gitlab.com/mauromamani20014/gin-auth/internal/auth"
	"gitlab.com/mauromamani20014/gin-auth/internal/entities"
)

type AuthRepository struct {
  db *gorm.DB
}

func NewAuthRepository(db *gorm.DB) auth.Repository {
  return &AuthRepository{db: db}
}

func (r *AuthRepository) Create(user *entities.User) (*entities.User, error) {
  if result := r.db.Create(user); result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.Create")
  }

  return user, nil
}

func (r *AuthRepository) GetById(userId uint) (*entities.User, error) {
  foundUser := &entities.User{}

  result := r.db.Where("user_id = ?", userId).First(foundUser)
  if result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.GetById")
  }

  return foundUser, nil
}

func (r *AuthRepository) GetByEmail(user *entities.User) (*entities.User, error) {
  foundUser := &entities.User{}

  result := r.db.Where("email = ?", user.Email).First(foundUser)
  if result.Error != nil {
    return nil, errors.Wrap(result.Error, "userRepository.GetByEmail")
  }

  return foundUser, nil
}

