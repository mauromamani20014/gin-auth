package middlewares

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/mauromamani20014/gin-auth/pkg/httpErrors"
	"gitlab.com/mauromamani20014/gin-auth/pkg/utils"
)

func (mw *MiddlewareManager) AuthJWTMiddleware() gin.HandlerFunc {
  return func(c *gin.Context) {
    cookie, err := c.Cookie("jwt")
    if err != nil {
      if errors.Is(err, http.ErrNoCookie) {
        c.JSON(http.StatusUnauthorized, httpErrors.NewUnauthorizedError(err))
        c.Abort()
      }
      c.JSON(http.StatusInternalServerError, httpErrors.NewInternalServerError(err))
      c.Abort()
    }

    claims, err := utils.ExtractJWT(cookie) 
    if err != nil {
      c.JSON(httpErrors.ErrorResponse(err))
      c.Abort()
    }
    uid := claims["id"].(string)
    userID, _ := strconv.ParseUint(uid, 10, 64)

    // find user
    user, err := mw.authUseCase.GetByID(uint(userID))
    if err != nil {
      c.JSON(httpErrors.ErrorResponse(err))
      c.Abort()
    }

    c.Set("user", user)
    c.Next()
  }
}
