package middlewares

import "gitlab.com/mauromamani20014/gin-auth/internal/auth"

type MiddlewareManager struct {
  authUseCase auth.UseCase
}

func NewMiddlewareManager(authUseCase auth.UseCase) *MiddlewareManager {
  return &MiddlewareManager{authUseCase: authUseCase}
}
