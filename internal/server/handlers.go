package server

import (
	"github.com/gin-gonic/gin"

	authHttp "gitlab.com/mauromamani20014/gin-auth/internal/auth/delivery/http"
	authRepo "gitlab.com/mauromamani20014/gin-auth/internal/auth/repository"
	authUC "gitlab.com/mauromamani20014/gin-auth/internal/auth/usecase"
	mw "gitlab.com/mauromamani20014/gin-auth/internal/middlewares"
)

func (s *Server) MapHandlers(r *gin.Engine) error {
  v1 := r.Group("/api/v1")
  authGroup := v1.Group("/auth")

  authRepository := authRepo.NewAuthRepository(s.db)
  authUseCase    := authUC.NewAuthUseCase(authRepository)
  authHandlers   := authHttp.NewAuthHandlers(authUseCase)

  middlewares := mw.NewMiddlewareManager(authUseCase)

  authHttp.MapAuthRoutes(authGroup, authHandlers, middlewares)

  return nil
}

