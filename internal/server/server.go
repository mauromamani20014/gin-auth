package server

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Server struct {
  gin *gin.Engine
  db  *gorm.DB
}

func NewServer(db *gorm.DB) *Server {
  return &Server{gin: gin.Default(), db: db}
}

func (s *Server) Run() error {
  if err := s.MapHandlers(s.gin); err != nil {
    return err
  }

  // Running Server
  if err := s.gin.Run(); err != nil {
    return err
  }

  return nil
}

