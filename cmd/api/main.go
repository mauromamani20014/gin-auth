package main

import (
	"log"

	"gitlab.com/mauromamani20014/gin-auth/internal/server"
	"gitlab.com/mauromamani20014/gin-auth/pkg/db/sqlite"
)

// TODO: Add loggers
func main() {
  // Connection to db
  db, err := sqlite.NewSqliteDB()
  if err != nil {
    log.Fatal(err)
  } else {
    log.Println("Database connected!")
  }

  sqlite.AutoMigrate(db)
  // New Server
  s := server.NewServer(db)

  if err := s.Run(); err != nil {
    panic(err)
  }
}


