package sqlite

import (
	"gitlab.com/mauromamani20014/gin-auth/internal/entities"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// NewDatabase: return new sqlite instance
func NewSqliteDB() (*gorm.DB, error) {
  db, err := gorm.Open(sqlite.Open("sqlite.db"), &gorm.Config{})
  if err != nil {
    return nil, err
  }

  return db, nil
}

func AutoMigrate(db *gorm.DB) {
  db.AutoMigrate(
    &entities.User{},
  )
}
