package utils

import (
	"github.com/gin-gonic/gin"
)

func ConfigureJWTCookie(c *gin.Context, token string) {
  c.SetCookie("jwt", token, 3600, "/", "localhost", false, true)
}

func DeleteJWTCookie(c *gin.Context) {
  c.SetCookie("jwt", "", -1, "/", "localhost", false, true)
}

